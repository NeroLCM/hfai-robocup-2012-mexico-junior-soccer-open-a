//Ӧ�ó���ģ�� 
#include "HardwareInfo.c"
#include "JMLib.c"
#include <SetLCDClear.h>
#include <SetLCDString.h>
#include <SetLED.h>
#include <SetLCDBack.h>
#include <SetLCD5CharSign.h>
#include <SetLCD5Char.h>
#include <SetLCD3Char.h>
#include <SetLCDString.h>
#include <SetMotor.h>
#include <SetNS.h>
#include <SetTenthS.h>
#include <SetCentiS.h>
#include <GetButton1.h>
#include <GetButton2.h>
#include <GetButton3.h>
#include <GetButton4.h>
#include <GetCompassB.h>
#include <GetSysTime.h>
#include <SetSysTime.h>
#include <GetADextend.h>
#include <SetBluetooth.h>
#include <GetBluetooth.h>
#include <SetAutoConnect.h>
#include <SetAlowConnect.h>
#include <GetBluetoothState.h>
#include <GetUltrasound.h>
#include <SetSwitch.h>
#include <GetCompoI3.h>
#include <GetRemoIR.h>


const char double_tank=1;
const float turn_force=0.38;//0.441;
int compass,com;
int left,right,front,back;//Us
const int leftmax=170+15,rightmax=180+15;
int lleft,lright,lfront=888;//legacy
int tdir=0;
const int spd=55,fullspeed=75;//could be 130 or 140
//int mspd=55;
 //SPD

int ecm,eim,eiml,ecml,eimr,ecmr,eif;

const int second=100;

//const int rsr=34,rdr=55,rdl=113,rsl=140;
//const int lsl=34,ldl=54,ldr=110,lsr=137;

const int lsl=27,ldl=43,ldr=70,lsr=87;
const int rsr=20,rdr=41,rdl=65,rsl=84;

const int bdb=10,bdf=27,bdtf=50,fdb=27,fdf=10;
const int lrsum=105;

const int a_balln=115,a_ballm=18-6;

int lax,lay;
int kick_count=0;

void initBT()
{
    SetAutoConnect(1);
    SetAlowConnect(1);
    //GetBluetoothState();
}

int rand(int limit)
{
unsigned long tmp;
tmp=GetSysTime()+front+left+compass+eiml+eimr+kick_count;
return tmp%limit;
}

void updateUs()
{
	left=GetUltrasound(_ULTRASOUND_l_);
	right=GetUltrasound(_ULTRASOUND_r_);
	front=GetUltrasound(_ULTRASOUND_f_);
	back=GetUltrasound(_ULTRASOUND_b_);
	if(back>=888)back=1;
	if(left>=leftmax+10)left=lleft;
	if(right>=rightmax+10)right=lright;
	if(front>=888)front=lfront;
	lleft=left;lright=right;
	lfront=front;
}

void setKick(int stat)
{
  SetSwitch(_SWITCH_1_,stat);
  if(stat){kick_count++;}
}

void setFL(int stat)
{
  SetSwitch(_SWITCH_fl_,stat);
}
void setFR(int stat)
{
  SetSwitch(_SWITCH_fr_,stat);
}
void setF(int stat)
{
  setFL(stat);
  setFR(stat);
}
void setFRev()
{
  SetSwitch(_SWITCH_flg_,1);
  SetSwitch(_SWITCH_frg_,1);
  SetSwitch(_SWITCH_fl_,0);
  SetSwitch(_SWITCH_fr_,0);
}
void initF()
{
  SetSwitch(_SWITCH_flg_,0);
  SetSwitch(_SWITCH_frg_,0);
  SetSwitch(_SWITCH_fl_,0);
  SetSwitch(_SWITCH_fr_,0);
  SetSwitch(_SWITCH_pump_,1);
}
void resetF()
{
  initF();setF(0);
}

void initEye()
{
    GetCompoI3(_COMPOUNDEYE3_1_, 14);
    GetCompoI3(_COMPOUNDEYE3_2_, 14);
}

void updateEye_raw()
{
    eif = GetRemoIR(_FLAMEDETECT_1_);
    eimr=GetCompoI3(_COMPOUNDEYE3_1_, 9);
    eiml=GetCompoI3(_COMPOUNDEYE3_2_, 9);
    if(eimr>eiml)
    {
	eim=eimr;
	ecm=ecmr=GetCompoI3(_COMPOUNDEYE3_1_, 8);
	ecml=GetCompoI3(_COMPOUNDEYE3_2_, 8);
	ecm=8-ecm;
    }
    else
    {
        eim=eiml;
        ecm=ecml=GetCompoI3(_COMPOUNDEYE3_2_, 8);
        ecmr=GetCompoI3(_COMPOUNDEYE3_1_, 8);
        ecm=-ecm;
    }
    
        //????
    return;//compensation abandon?
    //if(ecm>6||ecm<-6)
	//ecm=GetCompoI3(_COMPOUNDEYE3_1_, 2)>GetCompoI3(_COMPOUNDEYE3_2_, 6)?7:-7;
   
}

void updateFMtr(int stat)
{
static int lstat=0;
static unsigned long ltime=0;
const int delay=12;// 1/8 second=one ball rotation

if(stat)
  {
    setF(1);
    ltime=GetSysTime();
    lstat=1;
  }
else
  {
  if(lstat==0)
    {
      //setF(0);
    }
  else
    {
      if(GetSysTime()>ltime+delay)
      {
        setF(0);lstat=0;
      }
      else
      {
        //setF(1);
      }
    }
  }
  
}

void updateEye()
{
  updateEye_raw();
  updateFMtr((ecm==1||ecm==-1)&&eim>95||(eif>>11>0));  //eif>2048
}



int updateCom()
{
return compass=GetCompassB(_COMPASS_1_);
}


void SmartSetMotor(unsigned long hid,int spd)
{
if(spd==0)
{SetMotor(hid,1,0);return;}
if(spd>100)spd=100;
if(spd<-100)spd=-100;
spd>0?
SetMotor(hid,0,spd):
SetMotor(hid,2,-spd);
}

void ApplyMotor(int m1,int m2,int m3,int m4)
{
SmartSetMotor(_MOTOR_1_, m1);
SmartSetMotor(_MOTOR_2_, m2);
SmartSetMotor(_MOTOR_3_, m3);
SmartSetMotor(_MOTOR_4_, m4);
}

void stop()
{
ApplyMotor(0,0,0,0);
}

void move_ad(int body,int dir,int spd)
{
/*
AI Smart Move procedure, Copyright 2012 HFAI. All rights reserved.
Includes SmartSetMotor, Copyright 2012 HFAI. Licensed under WTFPL.
last-modified: 2012-6-21
*/
const double degrad=0.0174532925;
const int pmin=5;
float ct1,ct2;
float x;
int m1,m2,m3,m4,compass;

 
compass=updateCom();

if(compass>361)
{
	while(compass>361)
	{
	ApplyMotor(15,15,15,15);
	SetCentiS(25);compass=updateCom();
	}
	ApplyMotor(0,0,0,0);
	return;
}

dir-=body;
while(dir>180)dir-=360;
while(dir<-180)dir+=360;

body-=compass;
while(body>180)body-=360;
while(body<-180)body+=360;

x=body*turn_force;//motor offset
if(spd==0&&x<2&&x>-2)x=0;


ct1=sin((45-dir)*degrad);
ct2=sin((dir+45)*degrad);

m1=-ct1*spd;
m2=-ct2*spd;
m3=-m1;
m4=-m2;


if(spd==0&&(x>-pmin&&x<pmin))x=0;

m1+=x;
m2+=x;
m3+=x;
m4+=x;

ApplyMotor(m1,m2,m3,m4);
}

void move(int dir,int spd)
{
move_ad(0,dir,spd);
}

void catch_towards(int dd)
{
	int dir,tdir;
	compass>180?(compass-=360):1;
	dir=compass+0*(compass-dd);//0.4
	if(ecm>1||ecm<-1)
	{	
		tdir=40;
		if(eim>120)tdir+=31;//***!!!
	
		if(ecm>0){dir+=tdir;}
		else{dir-=tdir;}
		dir+=ecm*19;
	}
	
	move_ad((compass+dd)/2, dir, spd);
}

void catch_orig(){
	int tdir,py;
	
	compass>180?(compass-=360):1;
	if(compass>50||compass<-50)
		{move(0,0);lax=0;lay=0;return;}

	py=compass*0.23;
	tdir=45;
	if(eim>128)tdir+=15;//***!!!
	
	if(ecm==1)
		{move_ad(compass+4+py,compass,spd);return;}
	if(ecm==-1)
		{move_ad(compass-4+py,compass,spd);return;}	
	
	catch_towards(0);return;
	/*
	if(ecm==2)
		{move_ad(compass+20,compass+tdir,spd);return;}
	if(ecm==-2)
		{move_ad(compass-20,compass-tdir,spd);return;}	
	if(ecm>2)
		{move_ad(compass+35,compass+tdir+12*ecm,spd);return;}	
	if(ecm<-2)
		{move_ad(compass-35,-compass-tdir+12*ecm,spd);return;}	
	stop();
	*/
}


void catchball()
{
static char lastchoice=0;
	if(lax==3&&lay==1)
	{
		if(lastchoice==0)
			{lastchoice=rand(3)+1;}
		if(lastchoice>1)
			{catch_towards(31);return;}
		else
			{catch_orig();return;}
	}
	if(lax==3&&lay==3)
	{
		if(lastchoice==0)
			{lastchoice=rand(3)+1;}
		if(lastchoice>1)
			{catch_towards(-31);return;}
		else
			{catch_orig();return;}
	}

	lastchoice=0;
	catch_orig();
}

void move_towards(int dir, int tspd)
{
const int edif=20;
//if(ecm>2||ecm<-2||eim<128){catch_towards(dir);return;}

	compass>180?(compass-=360):1;
	dir>180?(dir-=360):1;

	if(compass-dir>4)
	{
		if(ecm==2)
			{move_ad(compass,compass+76,tspd);return;}
		if(ecm==-2)
			{move_ad(compass-20,compass-42,tspd);return;}

			move_ad(compass-18,compass+6,tspd);

	}
	else if(compass-dir<-4)
	{
		if(ecm==-2)
			{move_ad(compass,compass-76,tspd);return;}
		if(ecm==2)
			{move_ad(compass+20,compass+42,tspd);return;}
		
			move_ad(compass+18,compass-6,tspd);

	}
	else {
		if(ecm==-2)
			{move_ad(dir,compass-42,tspd);return;}
		if(ecm==2)
			{move_ad(dir,compass+42,tspd);return;}
		move_ad(dir,dir,tspd);
	}
}

void moveball_close(int py,int tspd)
{
int ang;

	if(ecm<2&&ecm>-2)
	{
	        if(eif<750){ang=(eimr-eiml)*0.06;}
		else if(eiml>95&&eimr<40){ang=-80;}
		else if(eiml>78&&eimr<40){ang=-75;}
		else if(eiml>100&&eimr<90){ang=-42;}//-52
		
		else if(eiml<20&&eimr>95){ang=81;}	
		else if(eiml<40&&eimr>78){ang=66;}		
		else if(eiml<90&&eimr>100){ang=32;}	//40
			
		else if(eiml<40&&eimr<40){ang=(eimr-eiml-7)*0.12;}
		else if(eiml<90&&eimr<90){ang=(eimr-eiml-7)*0.32;}
		else {ang=(eimr*0.93-eiml)*0.18;}//both very close
		
		move_ad(py,py+ang,tspd);
		return;
	}
	
	if(ecm==-2)
	{
		if(eim<85){ang=-95;}
		else if(eim<130){ang=-115;}
		else {ang=-145;}
		move_ad(py,py+ang,tspd*0.85);
		return;
	}
	
	if(ecm>0)
	{	
		eim-=75;if(eim<0)eim=0;
		//ang=29*ecm-18;ang+=(int)(eim*0.7);
		ang=35*ecm+22;
		ang+=(int)(eim*0.225);
	}
	else
	{	
		eim-=60;if(eim<0)eim=0;
		//ang=29*ecm-18;ang+=(int)(eim*0.7);
		ang=32*ecm-2;
		ang-=(int)(eim*0.245);
	}	
	move_ad(0,ang,tspd);
}


void moveball_std(int tspd)
{
int ang;

	//updateEye();	
	
	//SetLCD5CharSign(6*8,0,ecm);
	//SetLCD5CharSign(6*8,2,eim);
	
		
	if(ecm<4&&ecm>-4){moveball_close(0,tspd);	return;}
	
	eim-=40;if(eim<0)eim=0;
	
	if(ecm>0)
	{
		ang=30*ecm-18;
		ang+=(int)(eim*0.50);
	}
	else
	{
		ang=30*ecm+18;
		ang-=(int)(eim*0.50);
	}	
	move_ad(0,ang,tspd);
	
	//SetLCD5CharSign(4*8,5,ang);
}


void moveball_py(int py,int tspd)
{
int ang;
	//SetLCD5CharSign(6*8,0,ecm);
	//SetLCD5CharSign(6*8,2,eim);
	
	if(ecm<2&&ecm>-2){moveball_close(py,tspd);return;}
	
	eim-=73;if(eim<0)eim=0;
	
	if(ecm>0)
	{
		ang=35*ecm-20;
		ang+=(int)(eim*0.63);
	}
	else
	{
		ang=35*ecm+20;
		ang-=(int)(eim*0.63);
	}	
	move_ad(py,py+ang,tspd);
}

void moveball_catch(int tspd)
{
	moveball_py(0,tspd);
}




const int py=0;
char violent_isgotball()
{
const int gotmax=110,sidegm=80,fgotmax=2470+30;//avg+30?
/*&&(abs(eiml-eimr)<10)*/
return 	
		(ecm==1||ecm==-1)
	&&
		(
			(
				//(eim>gotmax&&eif>fgotmax)
				(eiml>sidegm&&eimr>sidegm)
			||
				(eiml>143&&eimr>134)
			||
				(eimr>143&&eiml>134)
			)
		);
}



unsigned long ts_lastkick=0;
void violent_dokick()
{
	unsigned long ts_kick=0;
	const int kick_timeout=80,minfront=6;
	int kickspeed=fullspeed+15;
	
	//int i;
	front=GetUltrasound(_ULTRASOUND_f_);
	updateCom();
	if(compass>90&&compass<270)return;//don't betray yourself!
	
	if(kick_count<35)
	{
		if(front<500&&front>70)
		  {
		    kickspeed=120;
		  if(front>130)
		    kickspeed=140;
		  }
	}
	else
	{kickspeed=140;}
	 
		move_ad(compass,compass,fullspeed);
		move_ad(compass,compass,kickspeed);
		move_ad(compass,compass,kickspeed);
		move_ad(compass,compass,kickspeed);
		
	updateEye();
	if(ecm<-3||ecm>3)
		{
		SetLCDString(3,6,"kabd");return;}
	else{
		SetLCDString(3,6,"kick");
	}
	
	setF(0);
	
	updateUs();
	
	ts_kick=GetSysTime();

	if(front>minfront&&((ts_kick- ts_lastkick)>kick_timeout))
	{
	    setFRev();
	    move_ad(compass,compass,kickspeed);move_ad(compass,compass,kickspeed);
	    setKick(1);		    					
	    ts_kick+=7;
	    while(GetSysTime()<ts_kick)
	    {
		move_ad(compass,compass,kickspeed);
	    }
	    setKick(0);
	    ts_lastkick=ts_kick;
	    resetF();
	}
	else
	{    
	    ts_kick+=7;
	    SetLCDBack(0);					
	    while(GetSysTime()<ts_kick)
	        move_ad(compass,compass,kickspeed);	        
		setFRev();	
	    SetLCDBack(1);
	    resetF();
	}
	
	return;
	
	stop();
	while(!GetButton4());
	while(GetButton4());
	
	return;
}

void simpleviolentstupidkick()
{
	unsigned long ctime;
	int aim_area;
	int aim_angle;
	int move_angle;
	int loop,i;
	
	int kick_fix;
	
	//char i=0;char j=0; //multiply factor
	int us_tmp;
	
	int kick_delay=13;
	const int goback_balllost_delay=15;
	const int goback_cor_delay=20;
	int cspd,f_limit;
	int radius;
	
	updateUs();	
	setF(1);
	
	us_tmp=left-right;
	kick_fix=kick_count>20?(kick_count-20)*0.22:0;
	
	SetLCDString(3,3,"svsk");
	if((front<=45&&(left<55||right<55)&&rand(100)<95)||(front<=55&&rand(100)<15))//liumang, side 95% and middle 15% possibility
	{	
		SetLCDString(3,5,"s_flm");
		aim_angle=(us_tmp>0)?1:-1;
		move_angle=(us_tmp>0)?180+30:180-30;
		//front=GetUltrasound(_ULTRASOUND_f_);
		//while(GetSysTime()<ctime)
		
		cspd=spd;
		while(cspd>10)move_ad(compass,compass,cspd*=0.7);
		cspd=0;ctime=GetSysTime()+goback_balllost_delay;
		//f_limit=50+rand(50)+rand(50);
		f_limit=50+rand(40)-rand(10);
		while(1)
			{
			SetLCDString(0,0,"w_sflm");
			move_ad(cspd*aim_angle*0.5,move_angle,cspd*0.5);
			if(cspd<60)cspd++;
			
			updateEye();
			front=GetUltrasound(_ULTRASOUND_f_);
			if(violent_isgotball()||eif>2000||(eiml>100&&eimr>100&&(eiml>140||eimr>140)))//(eif>1500&&eim>90)
				{
				ctime=GetSysTime()+goback_balllost_delay;
				}
			else
				{
				//ctime=0;
				}
			if(GetSysTime()>ctime)return;
			if(!(ecm==1||ecm==-1))return;
			if(eiml<60||eimr<60)return;			
			if(front>f_limit||back<10)break;
			} 
		//loop=1+rand(3);
		loop=1;
		radius=35;
		ctime=GetSysTime();
		if(back>55&&back<105&&rand(100)<80)
		while(1)
			{
			updateEye();
			if(ecm>1||ecm<-1||eif<1100)return;
			move_ad(compass+aim_angle*28,compass+aim_angle*28-(90-20)*aim_angle,radius);
			if(GetSysTime()>ctime+800)return;
			if(loop==1&&compass>175&&compass<185)loop++;
			if(loop==2&&(compass>340||compass<20))return;
			}
		else
		if(rand(100)<80&&back>50)
		for(cspd=60;cspd<(720-0*2);cspd++)
			{
			for(i=0;i<loop;i++)
				{
				move_ad(cspd*aim_angle*0.5,cspd*aim_angle*0.5-(90-20)*aim_angle,radius);
				updateEye();
				}
			if(ecm>1||ecm<-1||eif<1100)return;
			}
		else		
		for(cspd=60;cspd>1;cspd-=1)
			move_ad(cspd*aim_angle*0.5,move_angle,cspd*0.5);
		
		
		move_ad(0,180,0);
		updateUs();
		return;
		
	}
	else if(front<90&&back>85)
	{
		SetLCDString(3,5,"sar_*2");
		if(us_tmp>53)
		{
			aim_angle=-30-rand(10+kick_fix);
		}
		else if(us_tmp<-53)
		{
			aim_angle=32+rand(10+kick_fix);		
		}
		else if(us_tmp>24)
		{
			if(rand(100)>65)
				aim_angle=5-rand(3);
			else aim_angle=-21-rand(4+kick_fix);			
		}
		else if(us_tmp<-24)
		{
			if(rand(100)>75)
				aim_angle=-5+rand(3);
			else aim_angle=16+rand(4+kick_fix);			
		}
		else //midd
		{
			aim_angle=rand(7)-7;
		}
	}
	else if(back>50||rand(100)<55)//area 4 -> area 3, 65% possibility
	{
		SetLCDString(3,5,"sar_*3");
		if(us_tmp>43)
		{
			aim_angle=-32-rand(9+kick_fix);
		}
		else if(us_tmp<-43)
		{
			aim_angle=26+rand(9+kick_fix);		
		}
		else if(us_tmp>24)
		{
			if(rand(10)>5)
				aim_angle=2-rand(7);
			else aim_angle=-15-rand(6+kick_fix);			
		}
		else if(us_tmp<-24)
		{
			if(rand(10)>5)
				aim_angle=-2+rand(7);
			else aim_angle=15+rand(6+kick_fix);			
		}
		else //midd
		{
			aim_angle=rand(8)-8;
		}
	}
	else
	{
		SetLCDString(3,5,"sar_*4");
		if(us_tmp>53)
		{
			aim_angle=-24-rand(10+kick_fix);
		}
		else if(us_tmp<-53)
		{
			aim_angle=24+rand(10+kick_fix);		
		}
		else if(us_tmp>24)
		{
			 aim_angle=-7-rand(12);			
		}
		else if(us_tmp<-24)
		{
			aim_angle=7+rand(12);			
		}
		else //midd
		{
			aim_angle=rand(8)-8;
		}
	}
	
	SetLCD5CharSign(80, 0, us_tmp);
	SetLCD5CharSign(80, 2, aim_angle);
	
	move_ad(compass,compass,spd);
	ctime=GetSysTime();
	ctime+=kick_delay;	
		while(GetSysTime()<ctime)
			{move_towards(aim_angle,spd+10);} 
			
	compass-=aim_angle;
	if(compass<0)compass+=360;
	if(compass>180)compass-=360;
	if(compass<0)compass=-compass;
	ctime+=compass*0.7651;
		while(GetSysTime()<ctime)
			{move_towards(aim_angle,spd+10);} 	
	
	violent_dokick();
}

void c_stop_a()
{
updateEye();
while((compass<10||compass>350)&&(eif<1000&&eim<3)){updateEye();updateCom();}
}

void gmidd()//gmidd_new for mexico
{
const int bsl=40;//139 53 139+55=194
const int fsl=110;//,fts=20,bts=32;

register char fb;

if(compass>60&&compass<300)
{move(0,0);return;}

fb=0;

//if(back<bdf&&front>fsl-30)fb=1;
//else 


if(back<bsl)//1:front 2:back
  {
	if(front<70)fb=0;
	else fb=1;	
  }
else if(front<fsl)
  {
  if(back>bsl+20)fb=2;
  else fb=0;
  }
//else if(back>bsl+25+30&&front>fsl)fb=2;//back>bsl+bdf+defence_bot
else fb=0;


 SetLCD3Char(80, 0, fb);
 
if(fb==2)
		{
			if(left<ldl+10&&right<rdr+10)
				{
				if(left<right)move(180-45,spd);
				 else move(180+45,spd);
				return;}
			if(left<ldl+10)
				{move(180-70,spd);return;}
			if(right<rdr+10)
				{move(180+70,spd);return;}
			if(left-right>40)
				{move(180+25,spd);return;}
			if(left-right>10)
				{move(180+10,spd);return;}
			if(left-right<-40)
				{move(180-25,spd);return;}
			if(left-right<-10)
				{move(180-10,spd);return;}
			//else
				{move(180,spd);return;}
			return;
		}
else if(fb==1)
		{
			if(left-right>40)
				{move(0-25,spd);return;}
			if(left-right>10)
				{move(0-10,spd);return;}
			if(left-right<-40)
				{move(0+25,spd);return;}
			if(left-right<-10)
				{move(0+10,spd);return;}

			move(0,spd);
			return;
		}
	//else (on stop line)
	if(left>ldr)
		{move(270,spd);return;}
	if(right>rdl)
		{move(90,spd);return;}
	if(left>ldl&&right<rdr)	
		{move(270,spd);return;}
	if(right>rdr&&left<ldl)
		{move(90,spd);return;}
	if(left>ldl&&right>rdr)//middle
		{
			/*
			if(left-right>20)
				{move(270,left-right);return;}
			if(right-left>20)
				{move(90,right-left);return;}
			*/
			//stop
			move(0,0);if(front>fsl&&back>bsl)c_stop_a();return;
		}
	//left<, right<;
	//being blocked!
	move(0,front-back);


}


/************2Beat Attack Evo************/
/* Function Name: 2 Beat Attack Evo (2Beat Attack Included)
/* Date: 2012.03.14
/* Author: chenxiaoqino@HFAI 2011
/                 NeroLCM@HFAI 2011
/* Description: A slightly complax attack strategy
/*************************************/

void atkevo_f()
{

	//py=0;

	if((ecm<=4&&ecm>=-4)||back>60)
		{moveball_std(fullspeed);return;}
		
	if(ecm==5)
	{move(135,fullspeed);return;}
	if(ecm==-5)
	{move(-135,fullspeed);return;}
	if(ecm==6)
	{move(170,spd+10);return;}
	if(ecm==-6)
	{move(-170,spd+10);return;}
	if(ecm==7||ecm==-7)
	{move((GetCompoI3(_COMPOUNDEYE3_1_, 2)>GetCompoI3(_COMPOUNDEYE3_2_, 6))?195:165,spd+10);return;}
	
	return;
}

void atkevo_m()
{
/*	
	if(1)
	if(front<28)
	if(back>120)
	if(compass<=10||compass>=350)
	{
		if(ecm==5||ecm==6)
		if(left<lsl)
			{py=3+rand(2);}//37+r10
		if(ecm==-5||ecm==-6)
		if(right<rsr)
			{py=-3-rand(4);}//-37-r10
	}
*/
	if(1)
	{
		//if(ecm==1||ecm==-1)
		//move_towards(0,mspd);
		//else
		moveball_std(spd);
	}
	else
	{
		if(ecm==1||ecm==-1)
		move_ad(py,py+ecm*5,spd);
		else
		moveball_py(py,spd);
	}
}

void atkevo_n()
{
	static unsigned long ts_follow=0;
	char ectmp;
	int aim_angle=0;
	
	if(1)
	{
		if(ecm<=3&&ecm>=-3)
		{
			ectmp=abs(ecm);
			
			if (ectmp==1)
			  if(violent_isgotball())
			  {simpleviolentstupidkick();return;}
			  /*else if((front<=60&&(left<55||right<55))&&rand(100)<85)||(front<=10&&rand(100)<15))
			  {moveball_std(20);return;}*/
			  
			moveball_std(spd);return;
			/*
			if (ectmp==1)
			{
				if(ts_follow==0)ts_follow=GetSysTime()+40;
				
				if(violent_isgotball()||GetSysTime()>ts_follow)
					{ts_follow=0;simpleviolentstupidkick();return;}
				else 
					{move_ad(py,(eimr-eiml)*0.15,mspd+10);return;}
			}
			
			ts_follow=0;
			
			if (ectmp==2)
			{
				aim_angle=89*(ecm>0?1:-1);
				move_ad(py,aim_angle,mspd-20);
				return;
			}
			else if (ectmp==3)
			{
				aim_angle=147*(ecm>0?1:-1);
				move(aim_angle,mspd-3);
			}*/
			return;
		}
		else //side&back
		{
		/*if((front<=60&&(left<55||right<55))&&rand(100)<85)||(front<=10&&rand(100)<15))
			  {moveball_std(20);return;}*/
		moveball_std(spd);return;
		}
	}
	else //py!=0
	{
		if(ecm==1||ecm==-1)
		{
			//py_kick
			if(violent_isgotball())
			{
				violent_dokick();return;
			}
			else
			{
				move_ad(compass,compass,fullspeed-19);
			}
		}
		else
		{
			moveball_py(py,spd);return;
		}
	}
}

char btnPressed()
{
return (GetRemoIR(_FLAMEDETECT_btn_)>4000);
}

void atkevo_dete()
{
register signed char lr,fb;

unsigned long ts_standby;

lr=0;fb=0;

updateUs();
 SetLCD3Char(0, 0, front);
 SetLCD3Char(0, 2, back);
 SetLCD3Char(0, 4, left);
 SetLCD3Char(0, 6, right);
updateEye();
 SetLCD3Char(40, 0, eim);
 SetLCD3Char(40, 2, ecm+100);
 SetLCD3Char(40, 4, eif/10);
updateCom();


    if(btnPressed())
	{
	stop();
	ts_standby=GetSysTime();
	SetLCDBack(0);
	while(btnPressed())
		{
		if(GetSysTime()>ts_standby+20)SetLCDBack(1);
		}
	SetLCDBack(1);
	updateUs();updateEye();updateCom();
	}



if(back<33)
{
	if(front>50)fb=2;
	else fb=1;
}
else if(back<48) fb=1;

if(left<lsl)
{
	if(right<rsr)//blocked
	{
	
	}
	else lr=-1;
}
else
{
	if(right<rsr)lr=1;
	else lr=0;
}

if(fb>0&&(ecm>2||ecm<-2)&&eim>=a_ballm)
{
	if(fb==2)//too back
	{
		if(eim>a_balln)
		{
			if(lr==0)//midd
			{
				if(ecm==7||ecm==-7)
					{move(0,0);return;}
				if(ecm==6)
					{move(90-15,spd);return;}
				if(ecm==-6)
					{move(-90+15,spd);return;}
				if(ecm==5)
					{
						if(back>5)move(180-15,spd);
						else move(90-25,spd);					
					return;}
				if(ecm==-5)
					{
						if(back>5)move(180+15,spd);
						else move(-90+25,spd);					
					return;}
				if(ecm==4)
					{move_ad(-20,90+5,spd);return;}
				if(ecm==-4)
					{move_ad(20,-90-5,spd);return;}
				if(ecm==3)
					{move(90,spd);return;}
				if(ecm==-3)
					{move(-90,spd);return;}				
			}
			else if(lr>0)//right //to be modified: ecm==5||ecm==6, get it out! ??
			{	
				if(ecm==7||ecm==-7)
					{move(0,0);return;}	
				if(ecm==6)
					{move(90-15,spd);return;}
				if(ecm==5)
					{move(90-15,spd);return;}
				if(ecm==4)
					{
						if(left>ldr)move(0,0);
						else move(90,spd);
					return;}
				if(ecm==3)
					{
						if(left>ldr+15)move(0,0);
						else move(90,spd-15);
					return;}
				if(ecm==-6)
					{move(-90+60,spd);return;}
				if(ecm==-5)
					{
						if(back>5)move(180+15,spd);
						else move(-90+45,spd);					
					return;}
				if(ecm==-4)
					{move_ad(20,-90-5,spd);return;}	
				if(ecm==-3)
					{move(-90,spd);return;}	
			}
			else//left
			{
			if(ecm==7||ecm==-7)
					{move(0,0);return;}	
				if(ecm==-6)
					{move(-90+15,spd);return;}
				if(ecm==-5)
					{move(-90+15,spd);return;}
				if(ecm==-4)
					{
						if(right>rdl)move(0,0);
						else move(-90,spd);
					return;}
				if(ecm==-3)
					{
						if(right>rdl+15)move(0,0);
						else move(-90,spd-15);
					return;}
				if(ecm==6)
					{move(90-60,spd);return;}
				if(ecm==5)
					{
						if(back>5)move(180-15,spd);
						else move(90-45,spd);					
					return;}
				if(ecm==4)
					{move_ad(-20,90+5,spd);return;}	
				if(ecm==3)
					{move(90,spd);return;}	
			}
			atkevo_n();return;//miss
		}
		else//eim<=a_balln(=115)
		{	
			if(lr==0)//midd
			{
			
				if(ecm==7||ecm==-7)
					{move(0,0);return;}
				if(ecm==6)
					{move(90-15,spd);return;}
				if(ecm==-6)
					{move(-90+15,spd);return;}
				if(ecm==5)
					{
						if(back>5)move(180-30,spd);
						else move(90-25,spd);					
					return;}
				if(ecm==-5)
					{
						if(back>5)move(180+30,spd);
						else move(-90+25,spd);					
					return;}
				if(ecm==4)
					{move(90+5,fullspeed);return;}
				if(ecm==-4)
					{move(-90-5,fullspeed);return;}
				if(ecm==3)
					{move(90-5,spd+5);return;}
				if(ecm==-3)
					{move(-90+5,spd+5);return;}
			}
			else if(lr>0)//right
			{	
				if(ecm==7||ecm==-7)
					{move(-90,0);return;}	
				if(ecm==6)
					{
						if(back>5)move(180+45,spd);
						else move(0,0);					
					return;}
				if(ecm==5)
					{
						if(back>5)move(180,spd);
						else move(0,0);					
					return;}
				if(ecm==4)
					{
						if(left>ldr)move(0,0);
						else move(90,spd);
					return;}
				if(ecm==3)
					{
						if(left>ldr+15)move(0,0);
						else move(90,spd+5);
					return;}
				if(ecm==-6)
					{move(-90+45,spd);return;}
				if(ecm==-5)
					{
						if(back>5)move(-90-45,spd);
						else move(-90+15,spd-10);					
					return;}
				if(ecm==-4)
					{move(-90-5,spd+10);return;}	
				if(ecm==-3)
					{move(-90,spd+5);return;}
			}
			else//left
			{
				if(ecm==7||ecm==-7)
					{move(90,0);return;}	
				if(ecm==-6)
					{
						if(back>5)move(180-45,spd);
						else move(0,0);					
					return;}
				if(ecm==-5)
					{
						if(back>5)move(180,spd);
						else move(0,0);					
					return;}
				if(ecm==-4)
					{
						if(right>rdl)move(0,0);
						else move(-90,spd);
					return;}
				if(ecm==-3)
					{
						if(right>rdl+15)move(0,0);
						else move(-90,spd+5);
					return;}
				if(ecm==6)
					{move(90-45,spd);return;}
				if(ecm==5)
					{
						if(back>5)move(90+45,spd);
						else move(90-15,spd-10);					
					return;}
				if(ecm==4)
					{move(90+5,spd+10);return;}	
				if(ecm==3)
					{move(90,spd+5);return;}			
			}
			atkevo_m();return;//miss
		}		
	}
	else//near back
	{
	if(eim>a_balln)
		{
			if(lr==0)//midd
			{
				if(ecm==7||ecm==-7)
					{move(0,0);return;}
				if(ecm==6)
					{move(180+45,spd);return;}
				if(ecm==-6)
					{move(180-45,spd);return;}
				if(ecm==5)
					{move(180+30,spd);return;}
				if(ecm==-5)
					{move(180-30,spd);return;}
				if(ecm==4)
					{move_ad(-10,90+5,fullspeed);return;}
				if(ecm==-4)
					{move_ad(10,-90-5,fullspeed);return;}
				//if(ecm==3||ecm==-3)
					{moveball_std(spd+5);return;}				
			}
			else if(lr>0)//right
			{					
				if(ecm==7||ecm==-7)
					{move(-90,0);return;}
				if(ecm==6)
					{move(180+45,spd);return;}
				if(ecm==-6)
					{move(180-45,spd);return;}
				if(ecm==5)
					{move(180+30,spd);return;}
				if(ecm==-5)
					{move(180-30,spd);return;}
				if(ecm==4)
					{
						if(left>ldr)move(0,0);
						else move(90,spd);
					return;}
				if(ecm==-4)
					{move_ad(20,-90+20,spd);return;}
				if(ecm==3)
					{
						if(back>40)moveball_std(spd);
						else if(left<ldr+15)move(90,spd);
						else move(0,10);//???;
					return;}
				if(ecm==-3)
					{
						if(back>40)moveball_std(spd);
						else move(-90+20,spd);
					return;}
			}
			else//left
			{
				if(ecm==7||ecm==-7)
					{move(90,0);return;}
				if(ecm==6)
					{move(180+45,spd);return;}
				if(ecm==-6)
					{move(180-45,spd);return;}
				if(ecm==5)
					{move(180+30,spd);return;}
				if(ecm==-5)
					{move(180-30,spd);return;}
				if(ecm==-4)
					{
						if(right>rdl)move(0,0);
						else move(-90,spd);
					return;}
				if(ecm==4)
					{move_ad(-20,90-20,spd);return;}				
				if(ecm==-3)
					{
						if(back>40)moveball_std(spd);
						else if(right<rdl+15)move(-90,spd);
						else move(0,10);//???
					return;}
				if(ecm==3)
					{
						if(back>40)moveball_std(spd);
						else move(90-20,spd);
					return;}
			}
			atkevo_n();return;//miss
		}
	else//eim<=a_balln
		{	 
			if(lr==0)//midd
			{
				if(ecm==4)
					{move(90+15,fullspeed);return;}
				if(ecm==-4)//if(ecm=-4)//??!!
					{move(-90-15,fullspeed);return;}
				moveball_std(spd);return;
			}
			else if(lr>0)//right
			{	
				if(ecm==7||ecm==-7||ecm==6||ecm==5)
				{
					if(back>40)move(180+45,spd);
					else move(-90,0);						
				}
				if(ecm==4)
					{
						if(left>ldr)move(0,0);
						else move(90,spd);
					return;}
				if(ecm==3)
					{
						if(left>ldr+15)move(0,0);
						else moveball_std(spd);
					return;}				
				if(ecm==-6)
					{move(-90,spd);return;}
				if(ecm==-5)
					{
						if(back>60)move(-90-45,spd);
						else move(-90+15,spd);					
					return;}
				if(ecm==-4)
					{
						if(back>60)moveball_std(spd);
						else move(-90,spd);					
					return;}
				if(ecm==-3)
					{
						if(back>60)moveball_std(spd);
						else move(-90+10,spd);					
					return;}
			}
			else//left			
			{	
			
				
				if(ecm==7||ecm==-7||ecm==-6||ecm==-5)
				{
					if(back>40)move(180-45,spd);
					else move(90,0);						
				}
				if(ecm==-4)
					{
						if(right>ldr)move(0,0);
						else move(-90,spd);
					return;}
				if(ecm==-3)
					{
						if(right>ldr+15)move(0,0);
						else moveball_std(spd);
					return;}				
				if(ecm==6)
					{move(90,spd);return;}
				if(ecm==5)
					{
						if(back>60)move(90+45,spd);
						else move(90-15,spd);					
					return;}
				if(ecm==4)
					{
						if(back>60)moveball_std(spd);
						else move(90,spd);					
					return;}
				if(ecm==3)
					{
						if(back>60)moveball_std(spd);
						else move(90-10,spd);					
					return;}	
			}
			atkevo_m();return;//miss
		}
	}
	moveball_std(spd);
	return;
}

if(back>65&&back<185&&(front>85||(ecm>5||ecm<-5)))
{
		if(eim>a_balln)
		{	
		if(ecm>3||ecm<-3)
			{
			if(ecm==4)
				{move(180,fullspeed);return;}
			if(ecm==5)
				{moveball_std(spd);return;}
			if(ecm==6)
				{moveball_std(spd);return;}	
			if(ecm==-4)
				{move(180,fullspeed);return;}
			if(ecm==-5)
				{moveball_std(spd);return;}
			if(ecm==-6)
				{moveball_std(spd);return;}		
			if(ecm==7||ecm==-7)			
					{
					if(lr==1){move(180+85,spd);return;}
					if(lr==-1){move(180-85,spd);return;}
					}	
			}
		}
		else
		{		
		if(ecm>3||ecm<-3)
			{
			if(ecm==4)
				{move(90+57,fullspeed);return;}	
			if(ecm==5)
				{move(180,fullspeed);return;}	
			if(ecm==6)
				{moveball_std(fullspeed);return;}	
			if(ecm==-4)
				{move(-90-57,fullspeed);return;}	
			if(ecm==-5)
				{move(180,fullspeed);return;}	
			if(ecm==-6)
				{moveball_std(fullspeed);return;}	
			if(ecm==7||ecm==-7)			
					{
					if(lr==1){move(180+55,spd);return;}
					if(lr==-1){move(180-55,spd);return;}
					}	
			}				
		}
}


	if(eim<a_ballm)	//gback
		{
		gmidd();		
		return;}
		
	/*if(eim<95&&eim>50)
	if(back>110)
	if(ecm>4||ecm<-4)
		{moveball_std(spd);return;}*/
		
	if(eim<40)	//far
	{
		atkevo_f();
		return;
	}
	if(eim>129||violent_isgotball())	
	{
		atkevo_n();
		return;
	}	
	//else 
	{
		atkevo_m();
		return;
	}

 
}



int main(void)
{
	int st;
    X1RCU_Init();
    SetSysTime(0);    
    initEye();initF();
    
    SetLCDBack(1);
    
    while(1)
    {
    if(GetSysTime()>20000)//reset
	{
		SetSysTime(0);
		ts_lastkick=0;
		initEye();initF();
		SetLCDBack(0);SetLCDBack(1);SetLCDClear();
	}
    
    atkevo_dete();
    }   

 
}
