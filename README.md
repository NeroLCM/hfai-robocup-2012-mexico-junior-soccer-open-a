# What is this #
This is the code we used in Robocup 2012 Mexico, Junior Soccer Open A league.

# Platform #
This code works with JoinMax X1-RCU. Since JoinMax is supporting backward compatibility things, I assume that the code works with X2-RCU, X3-RCU, etc. as well. For older CPU/RCUs such as JoinMax XRCU, you'll have to modify the Hardware Info and still, it is very likely that the older RCUs run out of memory and die at run time.

You'll need four JoinMax ultrasonic distance sensors (analog output), two compound inferred sensors (I2C, we used JoinMax JMP-BE-1725) for ball detection, a pneumatic kicker (controlled by 1 input bit at 3.3v), a PWM motor control board and four motors. Our system operates at 12V input (3 Li-Po batteries in series).

# Understanding the code #
In the sense of Artificial Intelligent, this is a Simple Reflex Agent. As high school students we did not know things like neural network, training, machine learning, etc. Therefore this algorithm is **almost** deterministic except that in some scenarios we use probabilities when two decisions are almost as good in average (for example, strategy A is good against type 1 opponent but bad against type 2, and strategy B is the reverse). The code is very messy, but it can be divided in to three parts: what to do when the ball is far away, what if if near, and what if it's really near. The code then consider the position of the robot and the direction it is facing, the guessed positions of the opponents, etc.

The best piece of code in the codebase is 
```
#!c

void move_ad(int body, int dir, int spd);
```
It makes the robot move towards *dir* in speed *spd*, while keep facing *body*. This function allows the robot to do advanced movements like dribbling through the opponents.

# Demo #
See [this video](https://www.youtube.com/watch?v=yRv6A0DpB-Q). You may want to see the goal in 2:13, which involves dribbling using the **move_ad** mentioned above. This shocked most of our opponents!


# How to use the code #
This code works with Field A. This means that this code does not work "out of the box" any longer because Field A is discontinued after 2012. In addition, the rules and the field have changed so much that you'll need to do extensive change to the code before it can make reasonable move.

# Only 1 commit? #
We did not use version control tools when we wrote this code. We had a folder for every modification and we merge manually. Those backup folders were lost and this is the version that was used in (and won) the Grand Final.

# Warning #
Robocup Junior participants, please DO NOT USE ANY OF OUR CODE WITHOUT ACKNOWLEDGEMENT. A copy of our code was sent to Robocup Junior Committee and if you copy our code without acknowledgement, YOU WILL GET CAUGHT in the interview and YOU WILL BE DISQUALIFIED. **Winning is good, but learning new things from Robocup is the most important thing to do.**