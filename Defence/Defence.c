//Ӧ�ó���ģ�� 
#include <SetLCDClear.h>
#include <SetLCDString.h>
#include <SetLED.h>
#include "HardwareInfo.c"
#include "JMLib.c"
#include <SetLCDBack.h>
#include <SetLCD5CharSign.h>
#include <SetLCD5Char.h>
#include <SetLCD3Char.h>
#include <SetLCDString.h>
#include <SetMotor.h>
#include <SetNS.h>
#include <SetTenthS.h>
#include <SetCentiS.h>
#include <GetButton1.h>
#include <GetButton2.h>
#include <GetButton3.h>
#include <GetButton4.h>
#include <GetCompassB.h>
#include <GetSysTime.h>
#include <SetSysTime.h>
#include <GetADextend.h>
#include <SetBluetooth.h>
#include <GetBluetooth.h>
#include <SetAutoConnect.h>
#include <SetAlowConnect.h>
#include <GetBluetoothState.h>
#include <GetUltrasound.h>
#include <SetSwitch.h>
#include <GetCompoI3.h>
#include <GetRemoIR.h>


const char double_tank=1;
const float turn_force=0.38;//0.441;
int compass,com;
int left,right,front,back;//Us
const int leftmax=170+15,rightmax=180+15;
int lleft,lright,lfront=888;//legacy
int tdir=0;
const int spd=53,fullspeed=85;//could be 130 or 140
int mspd=50;
 //SPD

int ecm,eim,eiml,ecml,eimr,ecmr,eif;

const int second=100;

//const int rsr=34,rdr=55,rdl=113,rsl=140;
//const int lsl=34,ldl=54,ldr=110,lsr=137;
const int lsl=27,ldl=43,ldr=70,lsr=87;
const int rsr=20,rdr=41,rdl=65,rsl=84;
const int bdb=10,bdf=33,bdtf=55;
int fdb=33,fdf=10;
//forbidden line:35,51
const int lrsum=105;

const int a_balln=110,a_ballm=18-6;

int lax,lay;
int kick_count=0;

void initBT()
{
    SetAutoConnect(1);
    SetAlowConnect(1);
    //GetBluetoothState();
}

int rand(int limit)
{
unsigned long tmp;
tmp=GetSysTime()+front+left+compass+eiml+eimr+kick_count;
return tmp%limit;
}

void updateUs()
{
	left=GetUltrasound(_ULTRASOUND_l_);
	right=GetUltrasound(_ULTRASOUND_r_);
	front=GetUltrasound(_ULTRASOUND_f_);
	back=GetUltrasound(_ULTRASOUND_b_);
	if(back>=888)back=1;
	if(left>=leftmax+10)left=lleft;
	if(right>=rightmax+10)right=lright;
	if(front>=888)front=lfront;
	lleft=left;lright=right;
	lfront=front;
}


void setKick(int stat)
{
  SetSwitch(_SWITCH_1_,stat);
  if(stat){kick_count++;}
}

void setFL(int stat)
{
  SetSwitch(_SWITCH_fl_,stat);
}
void setFR(int stat)
{
  SetSwitch(_SWITCH_fr_,stat);
}
void setF(int stat)
{
  setFL(stat);
  setFR(stat);
}
void setFRev()
{
  SetSwitch(_SWITCH_flg_,1);
  SetSwitch(_SWITCH_frg_,1);
  SetSwitch(_SWITCH_fl_,0);
  SetSwitch(_SWITCH_fr_,0);
}
void initF()
{
  SetSwitch(_SWITCH_flg_,0);
  SetSwitch(_SWITCH_frg_,0);
  SetSwitch(_SWITCH_fl_,0);
  SetSwitch(_SWITCH_fr_,0);
}
void resetF()
{
  initF();setF(0);
}

void initEye()
{
    GetCompoI3(_COMPOUNDEYE3_1_, 14);
    GetCompoI3(_COMPOUNDEYE3_2_, 14);
}

void updateEye_raw()
{
    eif = GetRemoIR(_FLAMEDETECT_1_);
    eimr=GetCompoI3(_COMPOUNDEYE3_1_, 9);
    eiml=GetCompoI3(_COMPOUNDEYE3_2_, 9);
    if(eimr>eiml)
    {
	eim=eimr;
	ecm=ecmr=GetCompoI3(_COMPOUNDEYE3_1_, 8);
	ecml=GetCompoI3(_COMPOUNDEYE3_2_, 8);
	ecm=8-ecm;
    }
    else
    {
        eim=eiml;
        ecm=ecml=GetCompoI3(_COMPOUNDEYE3_2_, 8);
        ecmr=GetCompoI3(_COMPOUNDEYE3_1_, 8);
        ecm=-ecm;
    }
        //????
    //return;//compensation abandon?
    if(ecm>6||ecm<-6)
    {
	ecm=GetCompoI3(_COMPOUNDEYE3_1_, 2)>GetCompoI3(_COMPOUNDEYE3_2_, 6)?7:-7;
    }
}

void updateFMtr(int stat)
{
static int lstat=0;
static unsigned long ltime=0;
const int delay=12;// 1/8 second=one ball rotation

if(stat)
  {
    setF(1);
    ltime=GetSysTime();
    lstat=1;
  }
else
  {
  if(lstat==0)
    {
      //setF(0);
    }
  else
    {
      if(GetSysTime()>ltime+delay)
      {
        setF(0);lstat=0;
      }
      else
      {
        //setF(1);
      }
    }
  }
  
}

void updateEye()
{
  updateEye_raw();
  updateFMtr((ecm==1||ecm==-1)&&eim>95||(eif>>11>0));  //eif>2048
}



int updateCom()
{
return compass=GetCompassB(_COMPASS_1_);
}


void SmartSetMotor(unsigned long hid,int spd)
{
if(spd==0)
{SetMotor(hid,1,0);return;}
if(spd>100)spd=100;
if(spd<-100)spd=-100;
spd>0?
SetMotor(hid,0,spd):
SetMotor(hid,2,-spd);
}

void ApplyMotor(int m1,int m2,int m3,int m4)
{
SmartSetMotor(_MOTOR_1_, m1);
SmartSetMotor(_MOTOR_2_, m2);
SmartSetMotor(_MOTOR_3_, m3);
SmartSetMotor(_MOTOR_4_, m4);
}

void stop()
{
ApplyMotor(0,0,0,0);
}

void move_ad(int body,int dir,int spd)
{
/*
AI Smart Move procedure, Copyright 2012 HFAI. All rights reserved.
Includes SmartSetMotor, Copyright 2012 HFAI. Licensed under WTFPL.
last-modified: 2012-6-21
*/
const double degrad=0.0174532925;
const int pmin=5;
float ct1,ct2;
float x;
int m1,m2,m3,m4,compass;

 
compass=updateCom();

if(compass>361)
{
	while(compass>361)
	{
	ApplyMotor(15,15,15,15);
	SetCentiS(25);compass=updateCom();
	}
	ApplyMotor(0,0,0,0);
	return;
}

dir-=body;
while(dir>180)dir-=360;
while(dir<-180)dir+=360;

body-=compass;
while(body>180)body-=360;
while(body<-180)body+=360;

x=body*turn_force;//motor offset
if(spd==0&&x<2&&x>-2)x=0;


ct1=sin((45-dir)*degrad);
ct2=sin((dir+45)*degrad);

m1=-ct1*spd;
m2=-ct2*spd;
m3=-m1;
m4=-m2;



if(spd==0&&(x>-pmin&&x<pmin))x=0;

m1+=x;
m2+=x;
m3+=x;
m4+=x;

ApplyMotor(m1,m2,m3,m4);
}

void move(int dir,int spd)
{
move_ad(0,dir,spd);
}

void catch_towards(int dd)
{
	int dir,tdir;
	compass>180?(compass-=360):1;
	dir=compass+0*(compass-dd);//0.4
	if(ecm>1||ecm<-1)
	{	
		tdir=40;
		if(eim>120)tdir+=31;//***!!!
	
		if(ecm>0){dir+=tdir;}
		else{dir-=tdir;}
		dir+=ecm*19;
	}
	
	move_ad((compass+dd)/2, dir, spd);
}

void catch_orig(){
	int tdir,py;
	
	compass>180?(compass-=360):1;
	if(compass>50||compass<-50)
		{move(0,0);lax=0;lay=0;return;}

	py=compass*0.23;
	tdir=45;
	if(eim>128)tdir+=15;//***!!!
	
	if(ecm==1)
		{move_ad(compass+4+py,compass,spd);return;}
	if(ecm==-1)
		{move_ad(compass-4+py,compass,spd);return;}	
	
	catch_towards(0);return;
	/*
	if(ecm==2)
		{move_ad(compass+20,compass+tdir,spd);return;}
	if(ecm==-2)
		{move_ad(compass-20,compass-tdir,spd);return;}	
	if(ecm>2)
		{move_ad(compass+35,compass+tdir+12*ecm,spd);return;}	
	if(ecm<-2)
		{move_ad(compass-35,-compass-tdir+12*ecm,spd);return;}	
	stop();
	*/
}


void catchball()
{
static char lastchoice=0;
	if(lax==3&&lay==1)
	{
		if(lastchoice==0)
			{lastchoice=rand(3)+1;}
		if(lastchoice>1)
			{catch_towards(31);return;}
		else
			{catch_orig();return;}
	}
	if(lax==3&&lay==3)
	{
		if(lastchoice==0)
			{lastchoice=rand(3)+1;}
		if(lastchoice>1)
			{catch_towards(-31);return;}
		else
			{catch_orig();return;}
	}

	lastchoice=0;
	catch_orig();
}

void move_towards(int dir, int tspd)
{
const int edif=20;
//if(ecm>2||ecm<-2||eim<128){catch_towards(dir);return;}

	compass>180?(compass-=360):1;
	dir>180?(dir-=360):1;

	if(compass-dir>4)
	{
		if(ecm==2)
			{move_ad(compass,compass+76,tspd);return;}
		if(ecm==-2)
			{move_ad(compass-20,compass-42,tspd);return;}

			move_ad(compass-18,compass+6,tspd);

	}
	else if(compass-dir<-4)
	{
		if(ecm==-2)
			{move_ad(compass,compass-76,tspd);return;}
		if(ecm==2)
			{move_ad(compass+20,compass+42,tspd);return;}
		
			move_ad(compass+18,compass-6,tspd);

	}
	else {
		if(ecm==-2)
			{move_ad(dir,compass-42,tspd);return;}
		if(ecm==2)
			{move_ad(dir,compass+42,tspd);return;}
		move_ad(dir,dir,tspd);
	}
}

void moveball_close(int py,int tspd)
{
int ang;

	if(ecm<2&&ecm>-2)
	{
	        if(eif<750){ang=(eimr-eiml)*0.1;}
		else if(eiml>95&&eimr<40){ang=-80;}
		else if(eiml>78&&eimr<40){ang=-75;}
		else if(eiml>100&&eimr<90){ang=-35;}
		
		else if(eiml<20&&eimr>95){ang=85;}	
		else if(eiml<40&&eimr>78){ang=75;}		
		else if(eiml<90&&eimr>100){ang=35;}	
			
		else if(eiml<40&&eimr<40){ang=(eimr-eiml)*0.12;}
		else if(eiml<90&&eimr<90){ang=(eimr-eiml)*0.47;}
		else {ang=(eimr-eiml)*0.18;}		
		
		move_ad(py,py+ang,tspd);
		return;
	}
	
	
	if(ecm>0)
	{	
		eim-=75;if(eim<0)eim=0;
		//ang=29*ecm-18;ang+=(int)(eim*0.7);
		ang=35*ecm+8;
		ang+=(int)(eim*0.225);
	}
	else
	{	
		eim-=60;if(eim<0)eim=0;
		//ang=29*ecm-18;ang+=(int)(eim*0.7);
		ang=30*ecm-15;
		ang-=(int)(eim*0.245);
	}	
	move_ad(0,ang,tspd);
}


void moveball_std(int tspd)
{
int ang;

	//updateEye();	
	
	//SetLCD5CharSign(6*8,0,ecm);
	//SetLCD5CharSign(6*8,2,eim);
	
		
	if(ecm<4&&ecm>-4){moveball_close(0,tspd);	return;}
	
	eim-=40;if(eim<0)eim=0;
	
	if(ecm>0)
	{
		ang=32*ecm-10;//-13,0.50
		ang+=(int)(eim*0.35);
	}
	else
	{
		ang=32*ecm+10;
		ang-=(int)(eim*0.35);
	}	
	move_ad(0,ang,tspd);
	
	//SetLCD5CharSign(4*8,5,ang);
}


void moveball_py(int py,int tspd)
{
int ang;
	//SetLCD5CharSign(6*8,0,ecm);
	//SetLCD5CharSign(6*8,2,eim);
	
	if(ecm<2&&ecm>-2){moveball_close(py,tspd);return;}
	
	eim-=73;if(eim<0)eim=0;
	
	if(ecm>0)
	{
		ang=32*ecm-20;
		ang+=(int)(eim*0.63);
	}
	else
	{
		ang=32*ecm+20;
		ang-=(int)(eim*0.63);
	}	
	move_ad(py,py+ang,tspd);
}

void moveball_catch(int tspd)
{
	moveball_py(0,tspd);
}




int py;
char violent_isgotball()
{
const int gotmax=110,sidegm=65,fgotmax=2470+50;//avg+50
/*&&(abs(eiml-eimr)<10)*/
return 	
		(ecm==1||ecm==-1)
	&&
		(
			(
				//(eim>gotmax&&eif>fgotmax)
				(eiml>sidegm&&eimr>sidegm&&eif>fgotmax)
			||
				(eiml>143&&eimr>131)
			||
				(eimr>143&&eiml>131)
			)
		);
}



unsigned long ts_lastkick=0;
void violent_dokick()
{
	unsigned long ts_kick=0;
	const int kick_timeout=255,minfront=6;
	int kickspeed=fullspeed;
	
	//int i;
	front=GetUltrasound(_ULTRASOUND_f_);
	
	if(front<500&&front>70)
	  {
	    kickspeed=100;
	  if(front>130)
	    kickspeed=140;
	  }
		move_ad(compass,compass,fullspeed);
		move_ad(compass,compass,kickspeed);
		move_ad(compass,compass,kickspeed);
		move_ad(compass,compass,kickspeed);
		
	updateEye();
	if(ecm<-3||ecm>3)
		{
		SetLCDString(3,6,"kabd");return;}
	else{
		SetLCDString(3,6,"kick");
	}
	
	setF(0);
	
	updateUs();
	
	ts_kick=GetSysTime();

	if(front>minfront&&((ts_kick- ts_lastkick)>kick_timeout))
	{
	    setFRev();
	    move_ad(compass,compass,kickspeed);move_ad(compass,compass,kickspeed);
	    setKick(1);		    					
	    ts_kick+=7;
	    while(GetSysTime()<ts_kick)
	    {
		move_ad(compass,compass,kickspeed);
	    }
	    setKick(0);
	    ts_lastkick=ts_kick;
	    resetF();
	}
	else
	{    
	    ts_kick+=7;
	    SetLCDBack(0);					
	    while(GetSysTime()<ts_kick)
	        move_ad(compass,compass,kickspeed);	        
		setFRev();	
	    SetLCDBack(1);
	    resetF();
	}
	
	
	return;
}

void simpleviolentstupidkick()
{
	unsigned long ctime;
	int aim_area;
	int aim_angle;
	int move_angle;
	
	//char i=0;char j=0; //multiply factor
	int us_tmp;
	
	int kick_delay=12;
	const int goback_balllost_delay=15;
	const int goback_cor_delay=20;
	int cspd;
	
		updateUs();
		/*us_tmp=left-right;
		if (us_tmp>41) {aim_area=-1;if(right<50)aim_area=-2;}
		else if (us_tmp<-41) {aim_area=1;if(left<50)aim_area=2;}
		else aim_area=0;

		if (front>96) us_tmp=10;
		else us_tmp=16;
	
	//rand(20) - 0-19
	aim_angle=aim_area*us_tmp+rand(20)-10;*/
	
	setF(1);
	
	us_tmp=left-right;
	
	SetLCDString(3,3,"svsk");
	if(front<90&&back>97)
	{
		SetLCDString(3,5,"sar_2");
		if(us_tmp>53)
		{
			aim_angle=-43-rand(20);
		}
		else if(us_tmp<-53)
		{
			aim_angle=46+rand(20);		
		}
		else if(us_tmp>24)
		{
			if(rand(10)>5)
				aim_angle=5-rand(3);
			else aim_angle=-27-rand(8);			
		}
		else if(us_tmp<-24)
		{
			if(rand(10)>5)
				aim_angle=-5+rand(3);
			else aim_angle=22+rand(8);			
		}
		else //midd
		{
			aim_angle=rand(7)-7;
		}
	}
	else if(back>85)
	{
		SetLCDString(3,5,"sar_3");
		if(us_tmp>53)
		{
			aim_angle=-22-rand(20);
		}
		else if(us_tmp<-53)
		{
			aim_angle=20+rand(20);		
		}
		else if(us_tmp>24)
		{
			if(rand(10)>5)
				aim_angle=5-rand(7);
			else aim_angle=-17-rand(8);			
		}
		else if(us_tmp<-24)
		{
			if(rand(10)>5)
				aim_angle=-5+rand(7);
			else aim_angle=17+rand(8);			
		}
		else //midd
		{
			aim_angle=rand(10)-10;
		}
	}
	else
	{
		SetLCDString(3,5,"sar_4");
		if(us_tmp>53)
		{
			aim_angle=-10-rand(5);
		}
		else if(us_tmp<-53)
		{
			aim_angle=10+rand(5);		
		}
		else if(us_tmp>24)
		{
			 aim_angle=-5-rand(7);			
		}
		else if(us_tmp<-24)
		{
			aim_angle=5+rand(7);			
		}
		else //midd
		{
			aim_angle=rand(8)-8;
		}
	}
	
	
	ctime=GetSysTime();
	ctime+=kick_delay+abs(aim_angle)*0.1;
		while(GetSysTime()<ctime)
			{move_towards(aim_angle,mspd+10);} 
	
	violent_dokick();
}

void c_stop_a()
{
updateEye();
while((compass<10||compass>350)&&(eif<1000&&eim<3)){updateEye();updateCom();}
}
/*
void gmidd()//gmidd_new for mexico
{
const int bsl=50;//139 53 139+55=194
const int fsl=190-50;//,fts=20,bts=32;

char fb=0;

if(compass>60&&compass<300)
{move(0,0);return;}

if(back<bsl)//1:front 2:back
  {
  if(front<fsl)//blocked
	{
	if(front<70)fb=0;
	else fb=1;		
	}
  else
	{
	fb=1;
	}
  }
else if(front<fsl)
  {
  if(back>bsl+20)fb=2;
  else fb=0;
  }
else if(back>bsl+25&&front>fsl)fb=2;
else fb=0;


if(fb==2)
		{
			if(left<ldl&&right<rdr)
				{move(180,spd);return;}
			if(left<ldl)
				{move(145,spd);return;}
			if(right<rdr)
				{move(215,spd);return;}
			if(left-right>40)
				{move(180+25,spd);return;}
			if(left-right>10)
				{move(180+10,spd);return;}
			if(left-right<-40)
				{move(180-25,spd);return;}
			if(left-right<-10)
				{move(180-10,spd);return;}
			//else
				{move(180,spd);return;}
		}
	if(fb==1)
		{
			if(left-right>40)
				{move(0-25,spd);return;}
			if(left-right>10)
				{move(0-10,spd);return;}
			if(left-right<-40)
				{move(0+25,spd);return;}
			if(left-right<-10)
				{move(0+10,spd);return;}
			//else
				{move(0,spd);return;}
		}
	//else (on stop line)
	if(left>ldr)
		{move(270,spd);return;}
	if(right>rdl)
		{move(90,spd);return;}
	if(left>ldl&&right<rdr)	
		{move(270,spd);return;}
	if(right>rdr&&left<ldl)
		{move(90,spd);return;}
	if(left>ldl&&right>rdr)//middle
		{
			
			//stop
			move(0,0);if(front>fsl&&back>bsl)c_stop_a();return;
		}
	//left<, right<;
	//being blocked!
	move(0,front-back);


}
*/

void gback()//a
{
signed char lr=0,fb=0;
int ang=0;

if(compass>45&&compass<315){move(0,0);return;}

if(back>21)fb=1;
else fb=0;

if(fb==0)
	{
	if(left-right>5)lr=1;
	if(left-right<-5)lr=-1;
	}
else if(back<50)
	{
	if(left-right>12)lr=1;
	if(left-right<-12)lr=-1;
	}
else
	{
	if(left-right>21)lr=1;
	if(left-right<-21)lr=-1;
	}
	
	
if(fb==0)
	{
	if(lr==0&&back<10){move(0,spd*0.5);return;}
	if(lr==0){move(0,0);return;}
	if(lr==-1)ang=90-7;
	if(lr==1)ang=-90+11;
	move(ang,spd);return;
	}
else
	{
	if(lr==0){
		if(back>70)move(180,spd*1.5);
		else if(back>30)move(180,spd);
		else move(180,spd*0.5);
	return;}
	if(lr==-1)ang=90+33;
	if(lr==1)ang=-90-33;
	move(ang,spd);return;
	}
}

void gback_b()
{
signed char lr=0,fb=0;//0-L-M-R, 0-F-M-B

if(compass>45&&compass<315){move(0,0);return;}

if(back<7+rand(3))//RP back
	{
	if(left>ldl-5&&right>rdr-5)
		{fb=2;if(back<4)fb=7;}
	else fb=4;
	}
else if(back<bdf+14)//33+14
	{
	if(left<ldl&&right<rdr)
		{fb=1;}
	else fb=6;
	}
else if(back<bdtf)//55
	{
		if(left>ldl&&right>rdr) fb=3;
		else fb=2;
	}
else fb=3;//back too far

if(fb==5||fb==2)
	{	
	if(left-right>10)lr=1;
	else if(right-left>10)lr=3;
	else lr=2;
	}
else if(fb==4)
	{
	lr=2;
	}
else if(fb==1)
	{
	lr=2;
	}
else if(fb==3)
	{
	if(left-right>25)lr=1;
	else if(right-left>25)lr=3;
	else lr=2;	
	}
else //fb==6
	{
	if(left-right>20)lr=1;
	else if(right-left>20)lr=3;
	else lr=2;	
	}

if(fb==6)
	{
	if(rand(100)<65)
		{
		if(lr==1) {move(-90-3,spd);return;}
		else if(lr==3) {move(90+5,spd);return;}
		}
	else
		{
		if(lr==1) {move(-90+60,spd);return;}
		else if(lr==3) {move(90-60,spd);return;}		
		}
	}
if(fb==7)
	{move(0,10);return;}
if(fb>3)fb-=3;
if(fb==1)
	{
	move(0,spd);move(0,spd);move(0,spd);
	}
else if(fb==2)
	{	
	if(lr==1) move(-90,spd);
	else if(lr==3) move(90,spd);
	else move(0,0);
	}
else//fb==3
	{
	if(back>144){
		move(180,fullspeed);//move(180,spd*0.7);
	return;}
	if(lr==1) move(180+65,spd*0.7);
	else if(lr==3) move(180-65,spd*0.7);
	else //straight back
		{
		if(back>bdtf+10)move(180,fullspeed);
		else if(back>31)move(180,spd*0.65);
		else move(180,spd*0.35);		
		}
	}
}


void def_midd()
{
register signed char lr,fb;

lr=0;//-1:left, 0:midd, 1:right
fb=0;//0:free, 1:caution, 2:never go back, better front 3:go front
/*
if(back<9)
	{
	fb=2;
	if(left<ldl&&right<rdr) //cautious?? (left<ldl||right<rdr)//
		fb=3;
	}
else if(back<bdf+14)//33+14
	{
	fb=1;
	if(left<ldl&&right<rdr)
		fb=3;
	}
else if(back<bdf+15)//48
	{
	fb=2;
	if(left>ldl&&right>rdr)
		fb=1;
	}
else if(back<bdtf)//55
	{
	fb=1;
	}
else fb=0;

if(fb==3)
	{
	if(ecm>3||ecm<-3)
		{move(0,0);}
	else if(ecm==3)
		{move(30,spd);}
	else if(ecm==-3)
		{move(-30,spd);}
	else if(ecm==2)
		{move(25,spd);}
	else if(ecm==-2)
		{move(-25,spd);}
	else moveball_std(spd);
	return;
	}*/
if(back<19)fb=2;
else if(back<25)fb=1;
else fb=0;
	
	
if(left<lsl)
	{
	if(right<rsr)//blocked
		{
		lr=0;
		}
	else lr=-1;
	}
else
	{
	if(right<rsr)lr=1;
	else lr=0;
	}
//if(left+right<127)lr=0;
	
if(fb==0)
	{
	if((ecm==7||ecm==-7))
		{
		if(lr==-1)move(90+15,spd);
		else if(lr==1)move(-90-15,spd);
		else moveball_std(spd);
		}
	else if(ecm>4||ecm<-4)//5,6
		moveball_std(fullspeed);
	else if(ecm==4||ecm==-4)
		move(180,spd*1.2);
	else if(back>35)//1,2,3 too front
		{
		if(lr==0)
			{
			if(ecm==1||ecm==-1&&eim>90)
				moveball_std(spd);
			else 
				{//move(180,spd);
				if(ecm>0)move(180-20,spd*1.5);
				else move(180+20,spd*1.5);
				}
			return;				
			}
		else if(lr==-1)
			{
			if(ecm>0)move(90+25,spd*1.5);
			else move(180+15,spd);
			return;
			}
		else//lr==1
			{			
			if(ecm<0)move(90+25,spd*1.5);
			else move(180-15,spd);
			return;
			}		
		}
	else if(back>22)//1,2,3 medium
		{
			if(ecm>1&&right>rdr)
				{move(90+15,fullspeed);}
			else if(ecm<-1&&left>ldl)
				{move(-90-15,fullspeed);}
				
			else if(eiml-eimr>20&&right<rdl)
				{move(-95,spd*0.5);}
			else if(eimr-eiml>20&&left<ldr)
				{move(95,spd*0.5);}
			else move(0,0);
			return;
		}
	else//1,2,3 near back
			{
			if(ecm>2)
				{move(90+10,fullspeed);}
			else if(ecm<-2)
				{move(-90-10,fullspeed);}
			else moveball_std(spd);
			}
	return;}

if(fb==1)
	{
	if(lr==0)
		{
			if(ecm==1||ecm==-1)
				{move(0,0);return;}
			if(ecm==2)
				{move(90+20,spd);return;}
			if(ecm==3)
				{
				if(right>rdr-5) move(90+5,fullspeed);
				else move(90+5,spd);
				return;}
			if(ecm==4)
				{move(90+15,fullspeed);return;}
			if(ecm==5)
				{move(90+50,spd);return;}
			if(ecm==6)
				{move(90+100,spd);return;}
			if(ecm==-2)
				{move(-90-20,spd);return;}
			if(ecm==-3)
				{
				if(left>ldl-5) move(-90-5,fullspeed);
				else move(-90-5,spd);
				return;}
			if(ecm==-4)
				{move(-90-15,fullspeed);return;}
			if(ecm==-5)
				{move(-90-50,spd);return;}
			if(ecm==-6)
				{move(-90-100,spd);return;}
			if(ecm==7||ecm==-7)
				{
				//if(eim<85)move(180,spd*0.3);
				//else 
				move(0,0);
				}			
		}
	else if(lr==-1)//left
		{
			if(ecm==-1||ecm==-2||ecm==-3)
				{move(0,0);return;}
			if(ecm==-4)
				{
				if(back>19)move(180+20,spd);
				else move(180,spd*0.5);
				return;}
			if(ecm==-5)
				{
				if(back>40)move(180,fullspeed);
				else move(180,spd);
				return;}
			if(ecm==-6)
				{
				if(back>45)move(180-45,spd);
				else move(180-75,spd);
				return;}
			if(ecm==1||ecm==7||ecm==-7)
				{move(90,spd);return;}
			if(ecm==2)
				{move(90+15,fullspeed);return;}
			if(ecm==3)
				{move(90+25,fullspeed);return;}
			if(ecm==4)
				{move(90+10,fullspeed);return;}
			if(ecm==5)
				{
				if(back>50)move(90+45,spd);
				else move(45,spd);
				return;}
			if(ecm==6)
				{move(90-15,0);return;}	
		}
	else//lr==1//right
		{		
			if(ecm==1||ecm==2||ecm==3)
				{move(0,0);return;}
			if(ecm==4)
				{
				if(back>19)move(180-20,spd);
				else move(180,spd*0.5);
				return;}
			if(ecm==5)
				{
				if(back>40)move(180,fullspeed);
				else move(180,spd);
				return;}
			if(ecm==6)
				{
				if(back>45)move(180+45,spd);
				else move(180+75,spd);
				return;}
			if(ecm==-1||ecm==7||ecm==-7)
				{move(-90,spd);return;}
			if(ecm==-2)
				{move(-90-15,fullspeed);return;}
			if(ecm==-3)
				{move(-90-25,fullspeed);return;}
			if(ecm==-4)
				{move(-90-10,fullspeed);return;}
			if(ecm==-5)
				{
				if(back>50)move(-90-45,spd);
				else move(-45,spd);
				return;}
			if(ecm==-6)
				{move(-90+15,0);return;}
		}
	}
else//fb==2, never back
	{
	if(lr==0)
		{
			if(ecm==7||ecm==-7)
				{move(0,0);return;}
			if(ecm==6)	
				{move(90-15,spd);return;}	
			if(ecm==5)	
				{
				if(left<ldl)move(90+20,spd);//A modify
				else move(90-35,spd);
				return;}	
			if(ecm==4)
				{move(90,fullspeed);return;}
			if(ecm==3)
				{
				if(right>rdr-5) move(90,fullspeed);
				else move(90,spd);
				return;}
			if(ecm==2)
				{move(90,spd*0.7);return;}
			if(ecm==-6)	
				{move(-90+15,spd);return;}	
			if(ecm==-5)	
				{
				if(right<rdr)move(-90-20,spd);//A modify
				else move(-90+35,spd);
				return;}	
			if(ecm==-4)
				{move(-90,fullspeed);return;}
			if(ecm==-3)
				{
				if(left>ldl-5) move(-90,fullspeed);
				else move(-90,spd);
				return;}
			if(ecm==-2)
				{move(-90,spd*0.7);return;}
			//if(ecm<2&&ecm>-2)
				{moveball_std(spd);return;}
		}
	else if(lr==-1)//left
		{	
			if(ecm==3)
				{move(90-3,fullspeed);return;}
			if(ecm==4)
				{move(90,fullspeed);return;}
			if(ecm>0)//if(ecm==1||ecm==2||ecm==3||ecm==5||ecm==6||ecm==7)
				{move(90,spd);return;}
			if(ecm==-4||ecm==-5)
				{move(-90-45,spd);return;}
			//A modify
			if(ecm==-6)
				{move(180,spd);return;}
			if(ecm==-3||ecm==-6||ecm==-7)
				{move(0,0);return;}
			if(ecm==-2)
				{move(0,spd);return;}
			if(ecm==-1)
				{moveball_std(spd);return;}
			
		}
	else//lr==1//right
		{	
			if(ecm==-3)
				{move(-90+3,fullspeed);return;}
			if(ecm==-4)
				{move(-90,fullspeed);return;}
			if(ecm<0)//if(ecm==-1||ecm==-2||ecm==-3||ecm==-5||ecm==-6||ecm==-7)
				{move(-90,spd);return;}	
			if(ecm==4||ecm==5)
				{move(90+45,spd);return;}	
			//A modify
			if(ecm==6)
				{move(180,spd);return;}
			if(ecm==3||ecm==6||ecm==7)
				{move(0,0);return;}//??stop 6
			if(ecm==2)
				{move(0,spd);return;}
			if(ecm==1)
				{moveball_std(spd);return;}
		}
	}
}

void def_near()
{
if(ecm<3&&ecm>-3)
	{
	if(violent_isgotball()){simpleviolentstupidkick();return;}
		if(back<40)moveball_std(spd);
		else
			{
			if(eiml-eimr>20&&right<rdl)
				{move(-92,spd*0.5);}
			else if(eimr-eiml>20&&left<ldr)
				{move(92,spd*0.5);}
			else move(0,0);
			}
	return;
	}
if(ecm==3)
	{
	if(left<ldr){move(90,spd);return;}
	
	if(back>40||(back>20&&(left>50||right>50)))
		move(180,5);//10??
	else
		move(0,0);
	return;
	}
if(ecm==-3)
	{
	if(right<rdl){move(-90,spd);return;}
	
	if(back>40||(back>20&&(left>50||right>50)))
		move(180,5);//10??
	else
		move(0,0);
	return;
	}
if(ecm==4)
	{
	if(left<ldr){
		if(back<30)move_ad(-8,90,fullspeed);
		else move_ad(-8,90+15,spd);	
	return;}
	
	if(back>40||(back>20&&(left>50||right>50)))
		move(90+70,spd*0.5);//10??
	else
		move(180,spd*0.5);
	return;
	}
if(ecm==-4)
	{
	if(right<rdl){
		if(back<30)move_ad(8,-90,fullspeed);
		else move_ad(8,-90-15,spd);
	return;}
	
	if(back>40||(back>20&&(left>50||right>50)))
		move(-90-70,spd*0.5);//10??
	else
		move(180,spd*0.5);
	return;
	}
if(ecm==5)
	{
	if(back>bdf+10)
		{move(180-15,spd);return;}	
	if(left<ldr-10)
		{move(90-45,spd);return;}
	//else if(back>40)
	//	{move(180,spd);return;}
	//else {move(0,0);return;}//??stop
	{move(180,spd);return;}//a modify
	}
if(ecm==6)
	{
	if(left<ldr)
		{move(90-60,spd);return;}
	//else if(back>40)
	//	{move(180,spd);return;}
	//else 
	//{move(0,0);return;}
	{move(180+15,spd);return;}
	}
if(ecm==-5)
	{
	if(back>bdf+10)
		{move(180+15,spd);return;}	
	if(right<rdl-10)
		{move(-90+45,spd);return;}
	//else if(back>40)
	//	{move(180,spd);return;}
	//else {move(0,0);return;}//??stop
	{move(180,spd);return;}//a modify
	}
if(ecm==-6)
	{
	if(right<rdl)
		{move(-90+60,spd);return;}
	//else if(back>40)
	//	{move(180,spd);return;}
	//else {move(0,0);return;}
	{move(180+15,spd);return;}//a modify
	}
if(ecm==7||ecm==-7)
	{
	if(left>ldl&&right>rdr)
		{if(eim<125)move(180,10);
		else move(0,0);
		return;}
	if(back>bdf+5)
		{
			if(left>ldr-10)
				{move_ad(-8,180-20,spd);return;}
			else if(right>rdl-10)
				{move_ad(8,180+20,spd);return;}
			else
				{move(180,spd);return;}
		}
	else
		{move(0,0);return;}
	}
}

char btnPressed()
{
return (GetRemoIR(_FLAMEDETECT_btn_)>4000);
}

void defence_dete()
{
unsigned long ts_standby;

updateUs();
 SetLCD3Char(0, 0, front);
 SetLCD3Char(0, 2, back);
 SetLCD3Char(0, 4, left);
 SetLCD3Char(0, 6, right);
updateEye();
 SetLCD3Char(40, 0, eim);
 SetLCD3Char(40, 2, ecm+100);
 SetLCD3Char(40, 4, eif/10);
updateCom();

    if(btnPressed())
	{
	stop();
	ts_standby=GetSysTime();
	SetLCDBack(0);
	while(btnPressed())
		{
		if(GetSysTime()>ts_standby+20)SetLCDBack(1);
		}
	SetLCDBack(1);
	updateUs();updateEye();updateCom();
	}

//catchball();return;

if(eim>115)
	{def_near();}
else if(eim>19)
	{def_midd();}
else
	{gback();}

}

int main(void)
{
    X1RCU_Init();
    SetSysTime(0);
    
    initEye();initF();
    
    SetLCDBack(1);
    
    while(1)
    {
    if(GetSysTime()>20000)//reset
	{
		SetSysTime(0);
		ts_lastkick=0;
		initEye();initF();
		SetLCDBack(0);SetLCDBack(1);SetLCDClear();
	}
	
    defence_dete();
    }   

 
}